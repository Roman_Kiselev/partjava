The configuration is stored in 'src/main/resources/application.properties'

System requirements:
1. Java 8, http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html
2. maven, https://maven.apache.org/download.cgi

For deploy it's necessary to perform the command in command line: 'mvn clean package'. After that, jar 'part-1.0.jar' will 
be generated in 'target' directory. To launch jar - execute the command 'java -jar part-1.0.jar'.


