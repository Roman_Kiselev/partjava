package com.domain;

public enum OrderDirection {
    ASC,
    DESC
}
