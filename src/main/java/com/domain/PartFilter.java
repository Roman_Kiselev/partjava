package com.domain;

import java.util.Date;

public class PartFilter {
    private Long id;
    private String partName;
    private String partNumber;
    private String vendor;
    private Integer quantity;
    private Date afterShipped;
    private Date beforeShipped;
    private Date afterReceived;
    private Date beforeReceived;
    private String orderField;
    private OrderDirection direction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getAfterShipped() {
        return afterShipped;
    }

    public void setAfterShipped(Date afterShipped) {
        this.afterShipped = afterShipped;
    }

    public Date getBeforeShipped() {
        return beforeShipped;
    }

    public void setBeforeShipped(Date beforeShipped) {
        this.beforeShipped = beforeShipped;
    }

    public Date getAfterReceived() {
        return afterReceived;
    }

    public void setAfterReceived(Date afterReceived) {
        this.afterReceived = afterReceived;
    }

    public Date getBeforeReceived() {
        return beforeReceived;
    }

    public void setBeforeReceived(Date beforeReceived) {
        this.beforeReceived = beforeReceived;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public OrderDirection getDirection() {
        return direction;
    }

    public void setDirection(OrderDirection direction) {
        this.direction = direction;
    }
}
