package com.controller;

import com.dao.PartDao;
import com.domain.PartFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping(value = "api/parts", produces = "application/json; charset=UTF-8")
public class PartController {
    private final PartDao partDao;

    @Autowired
    public PartController(PartDao partDao) {
        this.partDao = partDao;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Object getParts(){
        return partDao.getParts();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Object getPartsWithFilter(@RequestBody PartFilter filter){
        return partDao.getParts(filter);
    }
}

