package com.dao;

import com.domain.PartFilter;
import com.entity.Part;

import java.util.List;

public interface PartDao {
    List<Part> getParts();
    List<Part> getParts(PartFilter part);
    Part getPartById();
    Part createPart(Part part);
    Part updatePart(Part part);
}
