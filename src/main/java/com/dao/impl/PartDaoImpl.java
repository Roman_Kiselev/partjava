package com.dao.impl;

import com.dao.PartDao;
import com.domain.PartFilter;
import com.entity.Part;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class PartDaoImpl implements PartDao {
    private final SessionFactory factory;

    @Autowired
    public PartDaoImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Part> getParts() {
        return factory.getCurrentSession().createQuery("from Part").list();
    }

    @Override
    public List<Part> getParts(PartFilter part) {
        CriteriaBuilder builder = factory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Part> query = builder.createQuery(Part.class);
        Root<Part> root = query.from(Part.class);
        List<Predicate> predicates = new ArrayList<>();
        if (part.getPartName() != null) {
            predicates.add(builder.like(root.get("partName"), "%" + part.getPartName() + "%"));
        }
        if (part.getPartNumber() != null) {
            predicates.add(builder.like(root.get("partNumber"), "%" + part.getPartNumber() + "%"));
        }
        if (part.getVendor() != null) {
            predicates.add(builder.like(root.get("vendor"), "%" + part.getVendor() + "%"));
        }
        if (part.getQuantity() != null){
            predicates.add(builder.and(builder.greaterThanOrEqualTo(root.get("quantity"), part.getQuantity())));
        }
        if (part.getBeforeShipped() != null && part.getAfterShipped() != null){
            predicates.add(builder.and(builder.between(root.get("shipped"), part.getAfterShipped(), part.getBeforeShipped())));
        }
        if (part.getBeforeShipped() != null && part.getAfterShipped() == null){
            predicates.add(builder.and(builder.lessThanOrEqualTo(root.get("shipped"), part.getBeforeShipped())));
        }
        if (part.getBeforeShipped() == null && part.getAfterShipped() != null){
            predicates.add(builder.and(builder.greaterThanOrEqualTo(root.get("shipped"), part.getAfterShipped())));
        }
        if (part.getBeforeReceived() != null && part.getAfterReceived() != null){
            predicates.add(builder.and(builder.between(root.get("received"), part.getAfterReceived(), part.getBeforeReceived())));
        }
        if (part.getBeforeReceived() != null && part.getAfterReceived() == null){
            predicates.add(builder.and(builder.lessThanOrEqualTo(root.get("received"), part.getBeforeReceived())));
        }
        if (part.getBeforeReceived() == null && part.getAfterReceived() != null){
            predicates.add(builder.and(builder.greaterThanOrEqualTo(root.get("received"), part.getAfterReceived())));
        }
        List<Order> order = new ArrayList<>();
        if (part.getOrderField() != null){
            switch (part.getDirection()){
                case ASC:
                    order.add(builder.asc(root.get(part.getOrderField()))); break;
                case DESC:
                    order.add(builder.desc(root.get(part.getOrderField()))); break;
            }
        }
        query.where(predicates.toArray(new Predicate[]{})).orderBy(order.toArray(new Order[]{}));
        Query<Part> q = factory.getCurrentSession().createQuery(query).setReadOnly(true);
        return q.getResultList();
    }

    @Override
    public Part getPartById() {
        return null;
    }

    @Override
    public Part createPart(Part part) {
        factory.getCurrentSession().save(part);
        return part;
    }

    @Override
    public Part updatePart(Part part) {
        factory.getCurrentSession().update(part);
        return part;
    }
}
