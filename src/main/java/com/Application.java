package com;

import com.dao.PartDao;
import com.domain.OrderDirection;
import com.domain.PartFilter;
import com.entity.Part;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.List;

@SpringBootApplication( exclude={HibernateJpaAutoConfiguration.class} )
public class Application {
    public static void main(String[] arg){
        SpringApplication.run(Application.class, arg);
    }

    @Bean
    InitializingBean saveData(PartDao repository){
        return () -> {
            List<Part> parts = repository.getParts();
            if (parts == null || parts.size() == 0) {
                repository.createPart(new Part(1L, "56H212-01", "HPC BLADE 7", "CH-DAL", 64, new Date(), new Date()));
                repository.createPart(new Part(2L, "56H212-01", "HPC BLADE 7", "CH-DAL", 54, new Date(), new Date()));
                repository.createPart(new Part(3L, "56H212-01", "HPC BLADE 7", "CH-DAL", 34, new Date(), new Date()));
                repository.createPart(new Part(4L, "56H212-01", "HPC BLADE 7", "CH-DAL", 24, new Date(), new Date()));
                repository.createPart(new Part(5L, "56H212-01", "HPC BLADE 7", "CH-DAL", 4, new Date(), new Date()));
                repository.createPart(new Part(6L, "56H212-01", "HPC BLADE 7", "CH-DAL", 14, new Date(), new Date()));
            }
        };
    }
}
